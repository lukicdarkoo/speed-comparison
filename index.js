var debug = false;
var num = 10 * 1000; // 100K

if (debug == true) {
	num = 10;
}



// Make random array
var arr = [];
for (var i = 0; i < num; i++) {
	arr[i] = Math.random();
}


// Bubble sort
var t;
var swapped = true;
while (swapped == true) {
	swapped = false;
	for (var i = 1; i < num; i++) {
		if (arr[i-1] > arr[i]) {
			t = arr[i];
			arr[i] = arr[i-1];
			arr[i-1] = t;
			swapped = true;
		}
	}
}


// Print all
if (debug == true) {
	for (var i = 0; i < num; i++) {
		console.log(arr[i]);
	}
}

