import random

debug = False
num = 10 * 1000 # 10K

if debug == True:
	num = 10


# Make random array
arr = []
for i in xrange(0, num):
	arr.append(random.random())


# Bubble sort
t = 0
swapped = True
while swapped == True:
	swapped = False
	for i in xrange(1, num):
		if arr[i-1] > arr[i]:
			t = arr[i];
			arr[i] = arr[i-1];
			arr[i-1] = t;
			swapped = True;


# Print all
if debug == True:
	for i in xrange(0, num):
		print arr[i]
