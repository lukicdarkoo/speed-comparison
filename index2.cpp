#include <iostream>
#include <stdlib.h>

using namespace std;

int debug = false;
int num = 10 * 1000; // 10K
int arr[10 * 1000];

int main() {
	if (debug == true) {
		num = 10;
	}


	// Make random array
	for (auto i = 0; i < num; i++) {
		arr[i] = rand();
	}

	// Bubble sort
	int t = 0;
	int swapped = true;
	while (swapped == true) {
		swapped = false;
		for (int i = 1; i < num; i++) {
			if (arr[i-1] > arr[i]) {
				t = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = t;
				swapped = true;
			}
		}
	}

	// Print all
	if (debug == true) {
		for (int i = 0; i < num; i++) {
			cout << arr[i] << endl;
		}
	}

	return 0;
}
