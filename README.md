# Speed comparison
Bubble sort algorithm perfomance in different programming languages

## Run test
`./test.sh`

## My results
```
Testing JavaScript...

real	0m0.566s
user	0m0.556s
sys	0m0.008s


Testing C++...

real	0m1.447s
user	0m1.444s
sys	0m0.000s


Testing C++ with O3...

real	0m0.273s
user	0m0.272s
sys	0m0.000s


Testing Python...

real	0m32.557s
user	0m32.436s
sys	0m0.032s



E X T R A S
Testing C++ with static arrays...

real	0m0.579s
user	0m0.572s
sys	0m0.000s


Testing C++ with static arrays and O3...

real	0m0.246s
user	0m0.244s
sys	0m0.000s


Check this: http://stackoverflow.com/questions/5168718/what-blocks-ruby-python-to-get-javascript-v8-speed
```
