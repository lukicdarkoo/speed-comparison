#!/bin/bash

echo 'Testing JavaScript...';
time node index.js

echo 
echo

echo 'Testing C++...';
g++ index.cpp -o index -std=c++11
time ./index


echo
echo

echo 'Testing C++ with O3...';
g++ index.cpp -o index -O3 -std=c++11
time ./index

echo
echo

echo 'Testing Python...';
time python index.py

echo 
echo 
echo
echo 'E X T R A S'



echo 'Testing C++ with static arrays...';
g++ index2.cpp -o index2 -std=c++11
time ./index2


echo
echo

echo 'Testing C++ with static arrays and O3...';
g++ index2.cpp -o index2 -O3 -std=c++11
time ./index2


echo 
echo
echo 'Check this: http://stackoverflow.com/questions/5168718/what-blocks-ruby-python-to-get-javascript-v8-speed'
