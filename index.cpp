#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

auto debug = false;
auto num = 10 * 1000; // 10K




int main() {
	if (debug == true) {
		num = 10;
	}


	// Make random array
	vector<int> arr;
	arr.resize(num);
	for (auto i = 0; i < num; i++) {
		arr[i] = rand();
	}

	// Bubble sort
	auto t = 0;
	auto swapped = true;
	while (swapped == true) {
		swapped = false;
		for (auto i = 1; i < num; i++) {
			if (arr[i-1] > arr[i]) {
				t = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = t;
				swapped = true;
			}
		}
	}

	// Print all
	if (debug == true) {
		for (auto i = 0; i < num; i++) {
			cout << arr[i] << endl;
		}
	}

	return 0;
}
